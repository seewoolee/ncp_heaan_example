import argparse
import os

import numpy as np
import pandas as pd


def generate_basic(n: int = 16):
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    a = np.random.uniform(-1, 1, size=(n,))
    b = np.random.uniform(-1, 1, size=(n,))
    df = pd.DataFrame(
        {
            "a": a.tolist(),
            "b": b.tolist(),
        }
    )
    df.to_csv(os.path.join(cur_dir, "data/basic1.csv"), index=False)
    c = np.random.uniform(-1, 1, size=(n,))
    d = np.random.uniform(-1, 1, size=(n,))
    df = pd.DataFrame(
        {
            "c": c.tolist(),
            "d": d.tolist(),
        }
    )
    df.to_csv(os.path.join(cur_dir, "data/basic2.csv"), index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--name",
        type=str,
        default="basic",
        help="synthetic dataset name",
        choices=["basic"],
    )
    parser.add_argument("--num", type=int, default=16, help="number of rows")
    args = parser.parse_args()
    if args.name == "basic":
        generate_basic(args.num)
