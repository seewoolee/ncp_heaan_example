# Tutorials for NCP HEaaN Homomorphic Analytics

## Requirements

* python >= 3.6
* pi-heaan
* pandas
* numpy

## Installation 

```shell
pip install -r requirements.txt
```