import argparse
import os

import numpy as np
import heaan

from utils import check_same


# setup
params = heaan.Parameters()
context = heaan.Context(params)

key_dir_path = "./keys"
os.makedirs(key_dir_path, exist_ok=True)
sk = heaan.SecretKey(context)
pk = heaan.PublicKeyPack(context, sk, key_dir_path)
enc_key = pk.get_enc_key()
mult_key = pk.get_mult_key()

encryptor = heaan.Encryptor(context)
decryptor = heaan.Decryptor(context)
evaluator = heaan.HomEvaluator(context)


def add(num_slots: int):
    """Add two randomly generated ciphertexts

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2

    Raises:
        ValueError: When the results is wrong
    """
    arr1 = np.random.randn(num_slots)
    arr2 = np.random.randn(num_slots)
    msg1 = heaan.Message(arr1)
    msg2 = heaan.Message(arr2)
    ctxt1 = heaan.Ciphertext()
    ctxt2 = heaan.Ciphertext()
    encryptor.encrypt(msg1, enc_key, ctxt1)
    encryptor.encrypt(msg2, enc_key, ctxt2)
    ctxt_out = heaan.Ciphertext()
    evaluator.add(ctxt1, ctxt2, ctxt_out)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_same(arr1 + arr2, msg_out._data)


def sub(num_slots: int):
    """Subtract two randomly generated ciphertexts

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2

    Raises:
        ValueError: When the results is wrong
    """
    arr1 = np.random.randn(num_slots)
    arr2 = np.random.randn(num_slots)
    msg1 = heaan.Message(arr1)
    msg2 = heaan.Message(arr2)
    ctxt1 = heaan.Ciphertext()
    ctxt2 = heaan.Ciphertext()
    encryptor.encrypt(msg1, enc_key, ctxt1)
    encryptor.encrypt(msg2, enc_key, ctxt2)
    ctxt_out = heaan.Ciphertext()
    evaluator.sub(ctxt1, ctxt2, ctxt_out)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_same(arr1 - arr2, msg_out._data)


def mult(num_slots: int):
    """Multiplicate two randomly generated ciphertexts

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2

    Raises:
        ValueError: When the results is wrong
    """
    arr1 = np.random.randn(num_slots)
    arr2 = np.random.randn(num_slots)
    msg1 = heaan.Message(arr1)
    msg2 = heaan.Message(arr2)
    ctxt1 = heaan.Ciphertext()
    ctxt2 = heaan.Ciphertext()
    encryptor.encrypt(msg1, enc_key, ctxt1)
    encryptor.encrypt(msg2, enc_key, ctxt2)
    ctxt_out = heaan.Ciphertext()
    evaluator.mult(ctxt1, ctxt2, mult_key, ctxt_out)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_same(arr1 * arr2, msg_out._data)


def square(num_slots: int):
    """Compute square of randomly generated ciphertext

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2

    Raises:
        ValueError: When the result is wrong
    """
    arr = np.random.randn(num_slots)
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)
    ctxt_out = heaan.Ciphertext()
    evaluator.mult(ctxt, ctxt, mult_key, ctxt_out)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_same(arr * arr, msg_out._data)


def rotate(num_slots: int, rot_idx: int, rot_dir: str):
    """Rotate randomly generated ciphertext

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
        rot_idx (int): rotation index
        rot_dir (str): rotation direction, 'left' or 'right'

    Raises:
        ValueError: When the results is wrong
    """
    arr = np.random.randn(num_slots)
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)
    ctxt_out = heaan.Ciphertext()
    if rot_dir == "left":
        evaluator.left_rotate(ctxt, rot_idx, pk, ctxt_out)
        ans_arr = np.concatenate((arr[rot_idx:], arr[:rot_idx]))
    else:  # rot_dir == "right"
        evaluator.right_rotate(ctxt, rot_idx, pk, ctxt_out)
        ans_arr = np.concatenate((arr[:rot_idx], arr[rot_idx:]))
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_same(ans_arr, msg_out._data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--job",
        type=str,
        default="add",
        help="job name",
        choices=[
            "add",
            "sub",
            "mult",
            "square",
            "rotate",
        ],
    )
    parser.add_argument(
        "--num_slots",
        type=int,
        default=8192,
        help="number of slots in message",
    )
    parser.add_argument(
        "--rot_idx",
        type=int,
        help="rotation index",
    )
    parser.add_argument(
        "--rot_dir",
        type=str,
        default="left",
        help="direction of rotation",
        choices=["left", "right"],
    )
    args = parser.parse_args()

    if args.bootstrap:
        context.make_bootstrappable(context._params._log_degree - 1)
    if args.job == "add":
        add(args.num_slots)
    elif args.job == "sub":
        sub(args.num_slots)
    elif args.job == "mult":
        mult(args.num_slots)
    elif args.job == "square":
        square(args.num_slots)
    elif args.job == "rotate":
        rotate(args.num_slots, args.rot_idx, args.rot_dir)

    print(context)
