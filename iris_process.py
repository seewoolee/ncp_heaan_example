import pandas as pd


if __name__ == "__main__":
    df = pd.read_csv("./IRIS.csv")
    # binary
    df = df[df.species.isin(["Iris-setosa", "Iris-versicolor"])]
    # categorize
    df = df.replace("Iris-setosa", 0)
    df = df.replace("Iris-versicolor", 1)
    # train/test split, 0.8: 0.2
    df_train = pd.concat(
        [df[df.index.isin(range(40))], df[df.index.isin(range(50, 90))]],
        ignore_index=True,
    )
    df_test = pd.concat(
        [df[df.index.isin(range(40, 50))], df[df.index.isin(range(90, 100))]],
        ignore_index=True,
    )
    print(len(df_train), len(df_test))
    # save
    df_train.to_csv("./iris-train.csv", index=False)
    df_test.to_csv("./iris-test.csv", index=False)
