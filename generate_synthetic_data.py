import argparse

import numpy as np
import pandas as pd


def generate_basic(n: int = 10):
    a = np.random.randint(-10, 10, size=(n,))
    b = np.random.randint(-10, 10, size=(n,))
    add = a + b
    mult = a * b
    df = pd.DataFrame(
        {
            "a": a.tolist(),
            "b": b.tolist(),
            "add_true": add.tolist(),
            "mult_true": mult.tolist(),
        }
    )
    df.to_csv("./basic.csv", index=False)


def generate_logistic(n: int = 100):
    a = 0.235
    b = 0.711
    x = np.random.randn(n)
    err = np.random.randn(n) * 0.01
    y = a * x + b + err
    y = 1 / (1 + np.exp(-y))
    df = pd.DataFrame({"x": x.tolist(), "y": y.tolist()})
    df.to_csv("./logistic.csv", index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--name",
        type=str,
        default="basic",
        help="synthetic dataset name",
        choices=["basic", "logistic"],
    )
    parser.add_argument("--num", type=int, default=10, help="number of rows")
    args = parser.parse_args()
    if args.name == "basic":
        generate_basic(args.num)
    else:
        generate_logistic(args.num)
