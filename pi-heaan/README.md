# pi-heaan examples

## basic examples (`examples_basic.py`)

This script introduces basic operations in pi-heaan such as addition, subtraction, multiplication, square, and rotation. 

### example shell scripts

```shell
# addition
python examples_basic.py --job add --num_slots 8192

# subtraction
python examples_basic.py --job sub --num_slots 8192

# multiplication
python examples_basic.py --job mult --num_slots 8192

# square
python examples_basic.py --job square --num_slots 8192

# rotation
python examples_basic.py --job rotate --num_slots 8192 --rot_idx 1 --rot_dir left
```

*Note that rotation does not work properly when the rotation index is not a power of 2, which needs to be fixed.*

## boostrapping-required examples (`examples_boostrap.py`)

This script consists of examples that requires boostrapping, such as large-degree polynomial evaluation and approximations (inverse, square root, sign, sigmoid, relu). 

### example shell scripts

```shell
# polynomial evaluation
python examples_bootstrap.py --job polynomial --num_slots 8192 --poly_deg 30

# inverse
python examples_bootstrap.py --job inverse --num_slots 8192

# square root
python examples_bootstrap.py --job sqrt --num_slots 8192

# sqrt_inverse
python examples_bootstrap.py --job sqrt_inverse --num_slots 8192

# sign
python examples_bootstrap.py --job sign --num_slots 8192

# sigmoid
python examples_bootstrap.py --job sigmoid --num_slots 8192

# relu
python examples_bootstrap.py --job relu --num_slots 8192
```

## statiscal functions (`examples_statistic.py`)

This script includes various statistical functions operates on ciphertexts, such as mean, standard deviation, normalization, pearson correlation, and linear regression.

### example shell scripts

```shell
# mean
python examples_statistic.py --job average --num_slots 8192

# standard deviation
python examples_statistic.py --job std --num_slots 8192

# normalize
python examples_statistic.py --job normalize --num_slots 8192

# pearson correlation
python examples_statistic.py --job pearson_correlation --num_slots 8192

# linear regression
python examples_statistic.py --job linear_regression --num_slots 8192
```
