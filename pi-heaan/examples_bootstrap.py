import argparse
import os

import numpy as np
import heaan

from utils import check_almost_same

# setup
params = heaan.Parameters()
context = heaan.Context(params)
context.make_bootstrappable(context._params._log_degree - 1)

key_dir_path = "./keys"
os.makedirs(key_dir_path, exist_ok=True)
sk = heaan.SecretKey(context)
pk = heaan.PublicKeyPack(context, sk, key_dir_path)
enc_key = pk.get_enc_key()
mult_key = pk.get_mult_key()

encryptor = heaan.Encryptor(context)
decryptor = heaan.Decryptor(context)
evaluator = heaan.HomEvaluator(context)


def _check_level_and_bootstrap(
    keypack: heaan.PublicKeyPack,
    ctxt_in: heaan.Ciphertext,
    ctxt_out: heaan.Ciphertext,
    cost_per_iter: int,
):
    """Check level and boostrap if needed

    Args:
        keypack (heaan.PublicKeyPack): public key pack
        ctxt_in (heaan.Ciphertext): input ciphertext
        ctxt_out (heaan.Ciphertext): output (bootstrapped) ciphertext
        cost_per_iter (int): required cost (levels) for a single iteration
    """
    if ctxt_in.get_level() - cost_per_iter <= ctxt_in.get_min_level_for_bootstrap():
        evaluator.bootstrap(ctxt_in, keypack, ctxt_out)


def _poly_eval_ptxt(coeffs: np.ndarray, arr: np.ndarray) -> np.ndarray:
    """Plaintext polynomial evaluation

    Args:
        coeffs (np.ndarray): polynomial coefficients
        arr (np.ndarray): input array

    Returns:
        np.ndarray: output array
    """
    arr_tmp0 = np.zeros_like(arr)
    arr_tmp1 = np.zeros_like(arr)
    arr_tmp2 = np.zeros_like(arr)

    for idx, val in enumerate(coeffs):
        if idx == 0:
            coeff_zero = val
        elif idx == 1:
            arr_tmp1 = arr * val
        else:
            arr_tmp0 *= arr
            arr_tmp2 = arr_tmp0 * val
            arr_tmp1 += arr_tmp2

    arr_tmp1 += coeff_zero
    return arr_tmp1


def poly_eval(
    num_slots: int,
    poly_deg: int,
):
    """Homomorphic polynomial evaluation of randomly generated ciphertext & coefficients

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
        poly_deg (int): degree of polynomial
    """
    poly_coeffs = np.random.randn(poly_deg + 1).tolist()
    arr = np.random.randn(num_slots)
    msg = heaan.Message(arr)
    ctxt_in = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt_in)

    ctxt_tmp0, ctxt_tmp1, ctxt_tmp2 = (
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
    )
    msg = heaan.Message(np.zeros((ctxt_in.get_number_of_slots())))
    encryptor.encrypt(msg, enc_key, ctxt_tmp0)
    encryptor.encrypt(msg, enc_key, ctxt_tmp1)
    encryptor.encrypt(msg, enc_key, ctxt_tmp2)
    ctxt_out = heaan.Ciphertext()

    for idx, val in enumerate(poly_coeffs):
        if idx == 0:
            coeff_zero = val
        elif idx == 1:
            evaluator.mult(ctxt_in, val, ctxt_tmp1)
        else:
            evaluator.mult(ctxt_in, ctxt_tmp0, mult_key, ctxt_tmp0)
            evaluator.mult(ctxt_tmp0, val, ctxt_tmp2)
            evaluator.add(ctxt_tmp1, ctxt_tmp2, ctxt_tmp1)
        if idx != len(poly_coeffs) - 1:
            _check_level_and_bootstrap(pk, ctxt_tmp0, ctxt_tmp0, 1)
            _check_level_and_bootstrap(pk, ctxt_tmp1, ctxt_tmp1, 1)
            _check_level_and_bootstrap(pk, ctxt_tmp2, ctxt_tmp2, 1)

    evaluator.add(ctxt_tmp1, coeff_zero, ctxt_out)
    ans_arr = _poly_eval_ptxt(poly_coeffs, arr)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(ans_arr, msg_out._data)


def inverse(num_slots: int):
    """Inverse computation

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
    """
    arr = np.random.uniform(0.0, 2.0, size=(num_slots,))
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = heaan.Ciphertext()
    heaan.math.approx.inverse(evaluator, pk, ctxt, ctxt_out)
    ans_arr = 1 / arr
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(ans_arr, msg_out._data, err=1e-5)


def sqrt(num_slots: int):
    """Sqrt comptuation

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
    """
    arr = np.random.uniform(0.0, 2.0, size=(num_slots,))
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = heaan.Ciphertext()
    heaan.math.approx.sqrt(evaluator, pk, ctxt, ctxt_out)
    ans_arr = np.sqrt(arr)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(ans_arr, msg_out._data, err=1e-5)


def sqrt_inverse(num_slots: int):
    """Sqrt of inverse computation

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
    """
    arr = np.random.uniform(0.0, 2.0, size=(num_slots,))
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = heaan.Ciphertext()
    # initial value (y0) should be smaller than sqrt(3/max_input)
    # otherwise, the return value becomes negative
    heaan.math.approx.sqrt_inverse(evaluator, pk, ctxt, ctxt_out, y0=1.0)
    ans_arr = 1 / np.sqrt(arr)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(ans_arr, msg_out._data, err=1e-5)


def sign(num_slots: int):
    """Sign function computation

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
    """
    arr = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = heaan.Ciphertext()
    heaan.math.approx.sign(evaluator, pk, ctxt, ctxt_out)
    ans_arr = 2 * (arr > 0.0).astype(float) - 1
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(ans_arr, msg_out._data, err=1e-5)


def _sigmoid_ptxt(arr: np.ndarray) -> np.ndarray:
    """Sigmoid function computation with plaintext

    Args:
        arr (np.ndarray): input

    Returns:
        np.ndarray: sigmoid(input)
    """
    return 1 / (1 + np.exp(-arr))


def sigmoid(num_slots: int, is_wide: bool = False, depth: int = 40):
    """Approximated sigmoid computation with randomly generated ciphertext

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
        is_wide (bool, optional): Choose between `sigmoid` and `sigmoid_wise`.
                                  `sigmoid_wise` can control with wider range
                                  of inputs. Defaults to False.
        depth (int): depth for `sigmoid_wide`. Defaults to 40.
    """
    if is_wide:
        arr = np.random.randn(num_slots)
    else:
        arr = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)
    ctxt_out = heaan.Ciphertext()
    if is_wide:
        heaan.math.approx.sigmoid_wide(evaluator, pk, ctxt, depth, ctxt_out)
    else:
        heaan.math.approx.sigmoid(evaluator, pk, ctxt, ctxt_out)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    ans_arr = _sigmoid_ptxt(arr)
    check_almost_same(ans_arr, msg_out._data, err=0.05)


def _relu_ptxt(arr: np.ndarray) -> np.ndarray:
    """ReLU function computation with plaintext

    Args:
        arr (np.ndarray): input

    Returns:
        np.ndarray: relu(input)
    """
    return arr * (arr > 0.0)


def relu(num_slots: int):
    """Approximated relu computation with randomly generated ciphertext

    Args:
        num_slots (int): number of slots of ctxt, should be a power of 2
    """
    arr = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    msg = heaan.Message(arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)
    ctxt_out = heaan.Ciphertext()
    heaan.math.approx.relu(evaluator, pk, ctxt, ctxt_out)
    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    ans_arr = _relu_ptxt(arr)
    check_almost_same(ans_arr, msg_out._data, err=0.05)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--job",
        type=str,
        default="polynomial",
        help="job name",
        choices=[
            "polynomial",
            "inverse",
            "sqrt",
            "sqrt_inverse",
            "sign",
            "sigmoid",
            "sigmoid_wide",
            "relu",
        ],
    )
    parser.add_argument(
        "--num_slots",
        type=int,
        default=8192,
        help="number of slots in message",
    )
    parser.add_argument(
        "--poly_deg",
        type=int,
        default=30,
        help="polynomial degree",
    )
    parser.add_argument(
        "--sigmoid_wide_depth",
        type=int,
        default=40,
        help="depth for sigmoid_wide approximiation",
    )
    args = parser.parse_args()

    if args.job == "polynomial":
        poly_eval(args.num_slots, args.poly_deg)
    elif args.job == "inverse":
        inverse(args.num_slots)
    elif args.job == "sqrt":
        sqrt(args.num_slots)
    elif args.job == "sqrt_inverse":
        sqrt_inverse(args.num_slots)
    elif args.job == "sign":
        sign(args.num_slots)
    elif args.job == "sigmoid":
        sigmoid(args.num_slots, is_wide=False)
    elif args.job == "sigmoid_wide":
        sigmoid(args.num_slots, is_wide=True, depth=args.sigmoid_wide_depth)
    elif args.job == "relu":
        relu(args.num_slots)

    print(context)
