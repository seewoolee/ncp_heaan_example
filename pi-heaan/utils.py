from typing import Union

import numpy as np


def check_same(x1: Union[np.ndarray, float], x2: Union[np.ndarray, float]):
    """Check if two arrays/numbers are the same or not

    Args:
        x1, x2 (Union[np.ndarray, float]): input arrays/numbers.

    Raises:
        ValueError: When the inputs are different
    """
    if isinstance(x1, float) and isinstance(x2, float):
        if x1 != x2:
            raise ValueError("input numbers have different values")
    elif isinstance(x1, np.ndarray) and isinstance(x2, np.ndarray):
        if x1.shape != x2.shape:
            raise ValueError("input arrays have different shapes")
        max_err = np.max(np.abs(x1 - x2))
        if max_err > 0.0:
            raise ValueError("input arrays are different")
    else:
        raise ValueError(f"invalid input type: {type(x1)}, {type(x2)}")


def check_almost_same(
    x1: Union[np.ndarray, float], x2: Union[np.ndarray, float], err: float = 1e-8
):
    """Check if maximum difference of two arrays/numbers are smaller than threshold or not.

    Args:
        x1, x2 (Union[np.ndarray, float]): input arrays/numbers.
        err (float, optional): maximum threshold. Defaults to 1e-8.

    Raises:
        ValueError: When the inputs have difference larger than threshold
    """
    if isinstance(x1, float) and isinstance(x2, float):
        if abs(x1 - x2) >= err:
            raise ValueError(f"input numbers have large difference: {abs(x1 - x2)}")
    elif isinstance(x1, np.ndarray) and isinstance(x2, np.ndarray):
        if x1.shape != x2.shape:
            raise ValueError("input arrays have different shapes")
        max_err = np.max(np.abs(x1 - x2))
        if max_err >= err:
            raise ValueError(f"input arrays have large difference: {max_err}")
    else:
        raise ValueError(f"invalid input type: {type(x1)}, {type(x2)}")
