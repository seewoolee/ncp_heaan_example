"""
-----------------------------------------------------------------------------------------------------------------------------------------------------------------

본 샘플파일은 사용자가 HEaaN Platform에서 π-HEaaN을 사용하기 위해 업로드해야 하는 파일에 대한 가이드입니다.
먼저, 이 샘플은 π-HEaaN 사용법에 대한 가이드가 아님을 알려드립니다.
π-HEaaN에 대해서 보다 자세히 알고 싶으시다면 "π-HEaaN 다운로드"를 클릭하시거나 "https://pypi.org/project/pi-heaan/"를 확인해주세요.
해당 페이지는 π-HEaaN을 다운로드하는 방법 및 tutorial을 제공합니다.

HEaaN Platform에서 π-HEaaN을 사용하고 싶으시다면, 다음과 같이 파일을 작성해주세요.

1. "FROM_HERE_IMPORT_USER_MODULE" 아래에 사용하시고자 하는 module을 import 해주세요. 사용하실 수 있는 module은 "FROM_HERE_IMPORT USER_MODULE"아래의 module로 제한됩니다.
3. "FROM_HERE_USER_CODE" 아래에 함수를 작성해주세요.
4. "FROM_HERE_DECRYPT_YOUR_CIPHERTEXT" 아래에 결과물을 복호화해주세요.


---------------------------------------------------------------------------------------------------------------------------------------------------------------- """


from typing import List
import heaan
import argparse
import os
import sys
import timeit
import math

""" --------------------------------------------------------------- HEAAN_PARAMETERS ------------------------------------------------------------------------ """

params = heaan.Parameters()  # 파라미터를 생성합니다.
context = heaan.Context(params)  # 컨텍스트를 생성합니다.
secret_key = heaan.SecretKey(context)  # 비밀키를 생성합니다.
public_key_path = "./public_key_path"  # 공개키를 저장할 경로를 지정합니다.
public_key_pack = heaan.PublicKeyPack(
    context, secret_key, public_key_path
)  # 공개키를 생성합니다.
enc_key = public_key_pack.get_enc_key()  # 암호화키를 불러옵니다.
mult_key = public_key_pack.get_mult_key()  # 곱셈연산키를 불러옵니다.
context.make_bootstrappable(16)
eval = heaan.HomEvaluator(context)  # 암호문 연산기능입니다.
num_slots = int(params.get_degree() / 2)  # 암호문 슬롯 갯수입니다. - full depth기준 65536
encryptor = heaan.Encryptor(context)  # 암호화 인스턴스를 생성합니다.

""" --------------------------------------------------------------- UTILITY_FUNCTIONS ------------------------------------------------------------------------ """

# 해당 경로에 있는 csv파일의 col_name, block_idx에 해당하는 데이터를 암호화하여 가져옵니다.(output: heaan.Ciphertext()) - 실제 HEaaN에서는 암호화 된 컬럼을 가져오는 함수로 실행됩니다.
def load_block(path: str, col_name: str, block_idx: int):
    df = pandas.read_csv(path)
    df = df.fillna(0.0)
    df_col = df[col_name]
    if len(df_col) < block_idx * num_slots:
        print(f"[Error] No block corresponding to {block_idx}-th '{col_name}'-block")
    else:
        msg = heaan.Message([0] * num_slots)
        ciphertext = heaan.Ciphertext()
        for i in range(num_slots):
            idx = i + num_slots * block_idx
            if idx < len(df_col):
                msg[idx] = df_col[idx]
        encryptor.encrypt(msg, enc_key, ciphertext)
    return ciphertext


# 해당 경로에 있는 csv파일의 col_name, block_idx에 해당하는 데이터의 유효데이터 정보를 암호화하여 가져옵니다.(output: heaan.Ciphertext()) - 실제 HEaaN에서는 암호화 된 컬럼을 가져오는 함수로 실행됩니다.
def load_block_vbit(path, col_name, block_idx):
    df = pandas.read_csv(path)
    df = df.isnull()
    df_col = df[col_name]
    if len(df_col) < block_idx * num_slots:
        print(f"[Error] No block corresponding to {block_idx}-th '{col_name}'-block")
    else:
        msg = heaan.Message([0] * num_slots)
        ciphertext = heaan.Ciphertext()
        for i in range(num_slots):
            idx = i + num_slots * block_idx
            if idx < len(df_col):
                if df_col[idx] == 0:
                    msg[idx] = 1
                else:
                    msg[idx] = 0
        encryptor.encrypt(msg, enc_key, ciphertext)
    return ciphertext


# 해당 경로에 있는 csv파일의 col_name에 해당하는 데이터 컬럼의 총 블럭 개수를 리턴합니다.(output: int) - 실제 HEaaN에서는 암호화 된 컬럼의 총 블럭 개수를 리턴하는 함수로 실행됩니다.
def num_of_blocks(path, col_name):
    df = pandas.read_csv(path)
    df_col = df[col_name]
    return int(math.ceil(len(df_col) / num_slots))


# 해당 경로에 있는 csv파일의 행 개수를 리턴합니다.(output: int) - 실제 HEaaN에서는 암호화 테이블의 행 개수를 리턴하는 함수로 실행됩니다.
def num_of_rows(path):
    df = pandas.read_csv(path)
    return len(df)


# 해당 경로에 있는 csv파일의 col_name, block_idx에 해당하는 데이터를 메시지로 가져옵니다.(output: heaan.Message())
def load_message(path, col_name, block_idx):
    df = pandas.read_csv(path)
    df_col = df[col_name]
    if len(df_col) < block_idx * num_slots:
        print(f"[Error] No block corresponding to {block_idx}-th '{col_name}'-block")
    else:
        msg = heaan.Message([0] * num_slots)
        ciphertext = heaan.Ciphertext()
        for i in range(num_slots):
            idx = i + num_slots * block_idx
            if idx < len(df_col):
                msg[idx] = df_col[idx]
    return msg


# 암호문 덧셈 - 암호문과 상수/메시지/암호문을 더합니다.
def add(ctxt, add_value):
    ctxt_result = heaan.Ciphertext()
    if not type(ctxt) == heaan.Ciphertext:
        if not type(add_value) == heaan.Ciphertext:
            print("[Error] The first input must be a ciphertext.")
        else:
            ctxt_result = add(add_value, ctxt)
    else:
        eval.add(ctxt, add_value, ctxt_result)
    return ctxt_result


# 암호문 뺄셈 - 암호문과 상수/메시지/암호문을 뺍니다.
def sub(ctxt, sub_value):
    ctxt_result = heaan.Ciphertext()
    if not type(ctxt) == heaan.Ciphertext:
        if not type(sub_value) == heaan.Ciphertext:
            print("[Error] One of input must be a ciphertext.")
        else:
            ciphertext_zero = heaan.Ciphertext()
            message_zero = heaan.Message([0] * num_slots)
            encryptor.encrypt(message_zero, enc_key, ciphertext_zero)
            sub_tmp = sub(ciphertext_zero, sub_value)
            ctxt_result = add(sub_tmp, ctxt)
    else:
        eval.sub(ctxt, sub_value, ctxt_result)
    return ctxt_result


# 암호문 곱셈 - 암호문과 상수/메시지/암호문을 곱합니다.
def mult(ctxt, mult_value):
    ctxt_result = heaan.Ciphertext()
    if not type(ctxt) == heaan.Ciphertext:
        if not type(mult_value) == heaan.Ciphertext:
            print("[Error] The first input must be a ciphertext.")
        else:
            ctxt_result = mult(mult_value, ctxt)
    else:
        if not type(mult_value) == heaan.Ciphertext:
            eval.mult(ctxt, mult_value, ctxt_result)
        else:
            eval.mult(ctxt, mult_value, mult_key, ctxt_result)
    return ctxt_result


# 암호문 회전 - 암호문을 rot_idx만큼 왼쪽(위쪽)으로 회전합니다.
def rotate(ctxt, rot_idx):
    ctxt_result = heaan.Ciphertext()
    if not type(ctxt) == heaan.Ciphertext:
        print("[Error] The first input must be a ciphertext.")
    else:
        eval.left_rotate(ctxt, rot_idx, public_key_pack, ctxt_result)
    return ctxt_result


# 암호문 부트스트랩 - 암호문의 곱셈을 할 수 있는 횟수를 늘려줍니다.
# 범위 제한이 있습니다. - 암호문의 모든 데이터가 -1과 1 사이에 있어야합니다. 그렇지 않으면 부트스트랩 후 결과에 오차가 발생할 수 있습니다.
def bootstrap(ctxt):
    ctxt_result = heaan.Ciphertext()
    if not type(ctxt) == heaan.Ciphertext:
        print("[Error] The first input must be a ciphertext.")
    else:
        # eval.bootstrap(ctxt, public_key_pack, ctxt_result, False)
        eval.bootstrap(ctxt, public_key_pack, ctxt_result)
    return ctxt_result


""" ----------------------------------------------------------- FROM_HERE_IMPORT_USER_MODULE ------------------------------------------------------------------- """

# 이곳에서 user_code에 필요한 모듈들을 import 해주세요.

import math
import pandas
from random import random, randint

""" ----------------------------------------------------------- END_IMPORT_USER_MODULE ------------------------------------------------------------------- """


""" --------------------------------------------------------------- FROM_HERE_USER_CODE ------------------------------------------------------------------------ """


def add_in_column(input_path: str, input_col: str) -> heaan.Ciphertext:
    """Add numbers in a single column

    Args:
        input_path (str): input path.
        input_col (str): column name.

    Returns:
        heaan.Ciphertext: result ciphertext
    """
    ctxt_in = load_block(input_path, input_col, 0)
    num_slots = ctxt_in.get_number_of_slots()
    ctxt_out, ctxt_tmp = heaan.Ciphertext(ctxt_in), heaan.Ciphertext(ctxt_in)
    for i in range(int(math.log2(num_slots))):
        rot_idx = 2 ** i
        eval.left_rotate(ctxt_out, rot_idx, public_key_pack, ctxt_tmp)
        eval.add(ctxt_out, ctxt_tmp, ctxt_out)
    return ctxt_out
    


# 이곳에서 암호문을 사용하는 함수를 작성해주세요.
def user_code(path_list: List[str], col_list: List[str]) -> heaan.Ciphertext:
    """user-defined function.

    Args:
        path_list (List[str]): list of input paths.
        col_list (List[str]): list of column names.

    Returns:
        heaan.Ciphertext: result ciphertext.
    """
    return add_in_column(path_list[0], col_list[0])


# 리턴으로 얻은 암호문을 저장할 폴더명(pi-heaan에서는 파일명으로 저장됩니다),컬럼명을 입력해주세요. Default는 "pi_heaan_folder","pi_heaan_colname"입니다.
# output_path = "pi_heaan_folder"
cur_dir = os.path.dirname(os.path.realpath(__file__))
# output_path = "data/add_in_col_piheaan"
output_path = "add_in_col_piheaan"
output_column_name = "add_in_col"
# 유효 데이터 숫자를 입력해주세요. 암호문은 full depth기준 65536개의 데이터로 이루어져있습니다. 위에서부터 몇 개의 데이터까지 저장할 지 정해주세요. Default는 1 (맨 위의 데이터만)입니다.
column_rows = 16

""" --------------------------------------------------------------- END_USER_CODE ------------------------------------------------------------------------ """


""" --------------------------------------------------------------- MAIN_CODE ------------------------------------------------------------------------ """


def main(margs):
    heaan.set_num_threads(math.ceil(os.cpu_count() / 2))
    arg_parser = argparse.ArgumentParser("HEaaN.STAT")
    arg_parser.add_argument(
        "-n", "--number_of_input", required=True, help="the number of input data"
    )
    args = arg_parser.parse_known_args(margs)[0]
    input_idx = int(args.number_of_input)

    for m in range(input_idx):
        arg_parser.add_argument(
            "-i" + str(m + 1),
            "--input_file_path" + str(m + 1),
            required=True,
            help="file path" + str(m + 1),
        )
        arg_parser.add_argument(
            "-t" + str(m + 1),
            "--input_col_name" + str(m + 1),
            required=True,
            help="column name" + str(m + 1),
        )

    args = arg_parser.parse_known_args(margs)[0]

    path_list = list()
    col_list = list()
    dargs = args.__dict__
    for m in range(input_idx):
        path = str(dargs["input_file_path" + str(m + 1)]).strip()
        col_name = str(dargs["input_col_name" + str(m + 1)]).strip()
        path_list.append(path)
        col_list.append(col_name)
    
    ctxt_res = user_code(path_list, col_list)

    # --------------------------------------------------------------------------------------- #
    # ------------------------------------  복호화 및 저장  ------------------------------------ #
    # --------------------------------------------------------------------------------------- #

    decryptor = heaan.Decryptor(context)  # 복호화 인스턴스를 생성합니다.
    message_out = heaan.Message()  # 암호문을 복호화하기 전, 복호문으로 사용할 변수의 타입을 Message로 지정해줍니다.

    decryptor.decrypt(ctxt_res, secret_key, message_out)  # 암호문을 복호화합니다.

    # 디폴트 저장 위치 : csv_file_path1
    # 디폴트 데이터 길이 : the number of rows in csv_file_path1
    save_path = "./" + output_path + ".csv"
    new_col_name = output_column_name
    data_size = column_rows

    if os.path.isfile(save_path):
        df = pandas.read_csv(save_path)
        df_length = len(df)
        data_size = df_length
    else:
        df = pandas.DataFrame([])

    data_list = list()
    for idx in range(data_size):
        data_list.append(message_out[idx].real)
    df[new_col_name] = data_list
    df.to_csv(save_path, index=None)


if __name__ == "__main__":
    #
    # provide operation and its argument to main function
    #

    margs = sys.argv[1:]
    if len(margs) == 0:
        print("[Error] No input data")

    start = timeit.default_timer()
    main(margs)
    end = timeit.default_timer()

    print("time elapsed: " + str(int(end - start)))
    print(context)
