import argparse
import math
import os
from typing import Optional

import numpy as np
import heaan

from utils import check_almost_same

# setup
params = heaan.Parameters()
context = heaan.Context(params)
context.make_bootstrappable(context._params._log_degree - 1)

key_dir_path = "./keys"
os.makedirs(key_dir_path, exist_ok=True)
sk = heaan.SecretKey(context)
pk = heaan.PublicKeyPack(context, sk, key_dir_path)
enc_key = pk.get_enc_key()
mult_key = pk.get_mult_key()

encryptor = heaan.Encryptor(context)
decryptor = heaan.Decryptor(context)
evaluator = heaan.HomEvaluator(context)


def average_ctxt(
    ctxt: heaan.Ciphertext, data_num: Optional[int] = None
) -> heaan.Ciphertext:
    """Compute average of elements in an input ciphertext

    Args:
        ctxt (heaan.Ciphertext): input ciphertext.
        data_num (Optional[int]): number of elements in an original message. Default to None.

    Returns:
        heaan.Ciphertext: ctxt whose decrypted result equals to message all filled with
                          average value.
    """
    ctxt_out, ctxt_tmp = heaan.Ciphertext(ctxt), heaan.Ciphertext(ctxt)
    num_slots = ctxt.get_number_of_slots()
    for i in range(int(math.log2(num_slots))):
        rot_idx = 2 ** i
        evaluator.left_rotate(ctxt_out, rot_idx, pk, ctxt_tmp)
        evaluator.add(ctxt_out, ctxt_tmp, ctxt_out)
    if data_num is None:
        data_num = num_slots
    evaluator.mult(ctxt_out, 1 / data_num, ctxt_out)
    return ctxt_out


def average(num_slots: int):
    """Compute average of elements in a single ciphertext

    Args:
        num_slots (int): number of slots of ciphertext, not necessarily power of 2
    """
    pad_num_slots = 2 ** math.ceil(math.log2(num_slots))
    arr = np.random.randn(num_slots)
    pad_arr = np.pad(arr, (0, pad_num_slots - num_slots))
    msg = heaan.Message(pad_arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = average_ctxt(ctxt, data_num=num_slots)

    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(np.mean(arr), msg_out._data[0].real)


def var_ctxt(ctxt: heaan.Ciphertext, data_num: Optional[int]) -> heaan.Ciphertext:
    """Compute variance of an input ciphertext.

    Args:
        ctxt (heaan.Ciphertext): input ciphertext.
        data_num (Optional[int]): number of elements in an original message. Default to None.

    Returns:
        heaan.Ciphertext: ctxt whose decrypted result equals to message all filled with
                          variance.
    """
    if data_num is None:
        data_num = ctxt.get_number_of_slots()
    ctxt_square, ctxt_avg_square, ctxt_out = (
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
    )
    evaluator.mult(ctxt, ctxt, mult_key, ctxt_square)
    ctxt_avg = average_ctxt(ctxt, data_num)
    evaluator.mult(ctxt_avg, ctxt_avg, mult_key, ctxt_avg_square)
    ctxt_square_avg = average_ctxt(ctxt_square, data_num)
    evaluator.sub(ctxt_square_avg, ctxt_avg_square, ctxt_out)
    return ctxt_out


def std_ctxt(
    ctxt: heaan.Ciphertext, data_num: Optional[int] = None
) -> heaan.Ciphertext:
    """Compute standard deviation of elements in an input ciphertext.

    Args:
        ctxt (heaan.Ciphertext): input ciphertext.
        data_num (Optional[int]): number of elements in an original message. Default to None.

    Returns:
        heaan.Ciphertext: ctxt whose decrypted result equals to message all filled with
                          standard deviation value.
    """
    ctxt_out = var_ctxt(ctxt, data_num)
    heaan.math.approx.sqrt(evaluator, pk, ctxt_out, ctxt_out)
    return ctxt_out


def std(num_slots: int):
    """Compute standard deviation of elements in a single ciphertext

    Args:
        num_slots (int): number of slots of ciphertext, not necessarily power of 2
    """
    pad_num_slots = 2 ** math.ceil(math.log2(num_slots))
    arr = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    pad_arr = np.pad(arr, (0, pad_num_slots - num_slots))
    msg = heaan.Message(pad_arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = std_ctxt(ctxt)

    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same(np.std(arr), msg_out._data[0].real)


def normalize_ctxt(
    ctxt: heaan.Ciphertext, data_num: Optional[int] = None
) -> heaan.Ciphertext:
    """Normalize an input ciphertext

    Args:
        ctxt (heaan.Ciphertext): input ciphertext.
        data_num (Optional[int]): number of elements in an original message. Default to None.

    Returns:
        heaan.Ciphertext: normalized ciphertext.
    """
    if data_num is None:
        data_num = ctxt.get_number_of_slots()
    ctxt_square, ctxt_avg_square, ctxt_std_inv, ctxt_out = (
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
    )
    evaluator.mult(ctxt, ctxt, mult_key, ctxt_square)
    ctxt_avg = average_ctxt(ctxt, data_num)
    evaluator.mult(ctxt_avg, ctxt_avg, mult_key, ctxt_avg_square)
    ctxt_square_avg = average_ctxt(ctxt_square, data_num)
    evaluator.sub(ctxt_square_avg, ctxt_avg_square, ctxt_std_inv)
    heaan.math.approx.sqrt_inverse(evaluator, pk, ctxt_std_inv, ctxt_std_inv)
    evaluator.sub(ctxt, ctxt_avg, ctxt_out)
    evaluator.mult(ctxt_out, ctxt_std_inv, mult_key, ctxt_out)
    return ctxt_out


def normalize(num_slots: int):
    """Normalize ciphertext as (x - mean) / std

    Args:
        num_slots (int): number of slots of ciphertext, not necessarily power of 2
    """
    pad_num_slots = 2 ** math.ceil(math.log2(num_slots))
    arr = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    pad_arr = np.pad(arr, (0, pad_num_slots - num_slots))
    msg = heaan.Message(pad_arr)
    ctxt = heaan.Ciphertext()
    encryptor.encrypt(msg, enc_key, ctxt)

    ctxt_out = normalize_ctxt(ctxt, data_num=num_slots)

    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    check_almost_same((arr - np.mean(arr)) / np.std(arr), msg_out._data)


def cov_ctxt(
    ctxt1: heaan.Ciphertext, ctxt2: heaan.Ciphertext, data_num: Optional[int] = None
) -> heaan.Ciphertext:
    """Covariance of two input ciphertexts

    Args:
        ctxt1, ctxt2 (heaan.Ciphertext): Input ciphertexts
        data_num (Optional[int]): number of elements in an original message. Default to None.

    Returns:
        heaan.Ciphertext: ctxt whose decrypted result equals to message all filled with
                          covariance.
    """
    if data_num is None:
        data_num = ctxt1.get_number_of_slots()
    ctxt1_avg, ctxt2_avg, ctxt_mult_avg, ctxt_avg_mult, ctxt_out = (
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
        heaan.Ciphertext(),
    )
    evaluator.mult(ctxt1, ctxt2, mult_key, ctxt_mult_avg)
    ctxt_mult_avg = average_ctxt(ctxt_mult_avg, data_num)
    ctxt1_avg = average_ctxt(ctxt1, data_num)
    ctxt2_avg = average_ctxt(ctxt2, data_num)
    evaluator.mult(ctxt1_avg, ctxt2_avg, mult_key, ctxt_avg_mult)
    evaluator.sub(ctxt_mult_avg, ctxt_avg_mult, ctxt_out)
    return ctxt_out


def pearson_correlation(num_slots: int):
    """Pearson correlation of two ciphertexts

    Args:
        num_slots (int): number of slots of ciphertext, not necessarily power of 2
    """
    pad_num_slots = 2 ** math.ceil(math.log2(num_slots))
    arr1 = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    arr2 = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    pad_arr1 = np.pad(arr1, (0, pad_num_slots - num_slots))
    pad_arr2 = np.pad(arr2, (0, pad_num_slots - num_slots))
    msg1 = heaan.Message(pad_arr1)
    msg2 = heaan.Message(pad_arr2)
    ctxt1, ctxt2 = heaan.Ciphertext(), heaan.Ciphertext()
    encryptor.encrypt(msg1, enc_key, ctxt1)
    encryptor.encrypt(msg2, enc_key, ctxt2)

    ctxt_var_mult_inv, ctxt_out = heaan.Ciphertext(), heaan.Ciphertext()

    ctxt1_var = std_ctxt(ctxt1, num_slots)
    ctxt2_var = std_ctxt(ctxt2, num_slots)
    evaluator.mult(ctxt1_var, ctxt2_var, mult_key, ctxt_var_mult_inv)
    heaan.math.approx.inverse(evaluator, pk, ctxt_var_mult_inv, ctxt_var_mult_inv)
    ctxt_cov = cov_ctxt(ctxt1, ctxt2, num_slots)
    evaluator.mult(ctxt_cov, ctxt_var_mult_inv, mult_key, ctxt_out)

    msg_out = heaan.Message()
    decryptor.decrypt(ctxt_out, sk, msg_out)
    ans_arr = np.mean(arr1 * arr2) - np.mean(arr1) * np.mean(arr2)
    ans_arr /= np.std(arr1) * np.std(arr2)
    check_almost_same(ans_arr, msg_out._data[0].real)


def linear_regression(num_slots: int):
    """Linear regression with two ciphertexts

    Args:
        num_slots (int): number of slots of ciphertext, not necessarily power of 2
    """
    pad_num_slots = 2 ** math.ceil(math.log2(num_slots))
    arr_x = np.random.uniform(-1.0, 1.0, size=(num_slots,))
    arr_err = np.random.uniform(-0.005, 0.005, size=(num_slots,))
    pad_arr_x = np.pad(arr_x, (0, pad_num_slots - num_slots))
    a = np.random.uniform(-1.0, 1.0, (1,))[0]
    b = np.random.uniform(-1.0, 1.0, (1,))[0]
    arr_y = a * arr_x + b + arr_err
    pad_arr_y = np.pad(arr_y, (0, pad_num_slots - num_slots))
    msg_x = heaan.Message(pad_arr_x)
    msg_y = heaan.Message(pad_arr_y)
    ctxt_x, ctxt_y = heaan.Ciphertext(), heaan.Ciphertext()
    encryptor.encrypt(msg_x, enc_key, ctxt_x)
    encryptor.encrypt(msg_y, enc_key, ctxt_y)

    ctxt_a = cov_ctxt(ctxt_x, ctxt_y, num_slots)
    ctxt_var_inv_x = var_ctxt(ctxt_x, num_slots)
    heaan.math.approx.inverse(evaluator, pk, ctxt_var_inv_x, ctxt_var_inv_x)
    evaluator.mult(ctxt_a, ctxt_var_inv_x, mult_key, ctxt_a)
    ctxt_avg_x = average_ctxt(ctxt_x, num_slots)
    ctxt_b = average_ctxt(ctxt_y, num_slots)
    evaluator.mult(ctxt_a, ctxt_avg_x, mult_key, ctxt_avg_x)
    evaluator.sub(ctxt_b, ctxt_avg_x, ctxt_b)

    msg_out_a, msg_out_b = heaan.Message(), heaan.Message()
    decryptor.decrypt(ctxt_a, sk, msg_out_a)
    decryptor.decrypt(ctxt_b, sk, msg_out_b)
    check_almost_same(a, msg_out_a._data[0].real, err=0.05)
    check_almost_same(b, msg_out_b._data[0].real, err=0.05)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--job",
        type=str,
        default="average",
        help="job name",
        choices=[
            "average",
            "std",
            "normalize",
            "pearson_correlation",
            "linear_regression",
        ],
    )
    parser.add_argument(
        "--num_slots",
        type=int,
        default=8192,
        help="number of slots in message",
    )
    args = parser.parse_args()

    if args.job == "average":
        average(args.num_slots)
    elif args.job == "std":
        std(args.num_slots)
    elif args.job == "normalize":
        normalize(args.num_slots)
    elif args.job == "pearson_correlation":
        pearson_correlation(args.num_slots)
    elif args.job == "linear_regression":
        linear_regression(args.num_slots)

    print(context)
